import React, { Component } from 'react';

import {
  Button as UiButton, Icon, DropDownMenu, DropDownModal,
  View as UiView, Text as UiText
} from '@shoutem/ui';

export class NavMenu extends Component {

  constructor(props) {
    super(props);

    let forms = [
      { title: 'Debt', value: 'Debt' },
      { title: 'Education Funding', value: 'EducationFunding' },
      { title: 'Administration Cost', value: 'AdministrationCost' },
      { title: 'Charity', value: 'Charity' }
    ];

    this.state = {
      forms: forms,
      selected: {},
      collapsed: false
    };

    this.onPressBack = this.onPressBack.bind(this);
    this.onPressNext = this.onPressNext.bind(this);
    this.onPressSelect = this.onPressSelect.bind(this);

    this.collapsed = this.collapsed.bind(this);
    this.close = this.close.bind(this);
  }

  onPressBack() {
    this.props.onPressBack(this.state);
  }

  onPressNext() {
    this.props.onPressNext(this.state);
  }

  onPressSelect(selected) {
    this.setState({ selected });
    this.props.onPressSelect(selected);
  }

  collapsed() {
    this.setState({ collapsed: true });
  }

  close() {
    this.setState({ collapsed: false });
  }

  render() {
    return (
      <UiView styleName='horizontal space-between' style={{paddingVertical: 10, paddingHorizontal: 10}}>
        <UiView style={{borderBottomWidth: 1, borderColor: 'gray'}}>
          <UiButton onPress={this.onPressBack}>
            <Icon name="left-arrow" />
            <UiText>BACK</UiText>
          </UiButton>
        </UiView>

        <UiView style={{borderBottomWidth: 1, borderColor: 'gray'}}>
          <UiButton onPress={this.collapsed}>
            <UiText>ADD</UiText>
          </UiButton>

          <DropDownModal
            options={this.state.forms}
            selectedOption={this.state.selected}
            onOptionSelected={this.onPressSelect}
            titleProperty="title"
            valueProperty="value"
            visible={this.state.collapsed}
            onClose={this.close}
          />
        </UiView>

        <UiView style={{borderBottomWidth: 1, borderColor: 'gray'}}>
          <UiButton onPress={this.onPressNext}>
            <UiText>NEXT</UiText>
            <Icon name="right-arrow" />
          </UiButton>
        </UiView>
      </UiView>
    );
  }
}
