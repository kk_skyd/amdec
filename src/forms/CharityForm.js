import React, { Component } from 'react';
import { Modal, TouchableHighlight, View, Text, TextInput, StyleSheet, ScrollView } from 'react-native';

import { Screen, NavigationBar, Button, FormGroup, Caption, Divider,
  Heading, Title, Button as UiButton, Icon, DropDownMenu, DropDownModal,
  View as UiView, Text as UiText } from '@shoutem/ui';

import _ from 'lodash';
import { NavMenu } from '../helpers/NavMenu';
import SwitchButton from '../components/SwitchButton';

export class CharityForm extends Component {

  constructor(props) {
    super(props);

    let charities = [
      { title: 'Yearly Contribution', value: 'Yearly' },
      { title: 'Lump-sum Contribution', value: 'Lumpsum' },
    ];

    let charityYearlyContribution = '1000000';
    let charityYears = '1';
    let selected = charities[0];
    // Maping record customer
    if (typeof props.customer != 'undefined' && props.customer.id) {
      charityYearlyContribution = props.customer.charityYearlyContribution;
      charityYears = props.customer.charityYears;

      let selectedIndex = _.findIndex(charities, { value: props.customer.selected.value });
      selected = charities[selectedIndex];
    }

    this.state = {
      amount: 0,
      // Charity Allocation
      charityYearlyContribution: charityYearlyContribution,
      charityYears: charityYears,

      // Selected
      charities: charities,
      selected: selected,
      customer: props.customer,

      activeSwitch: 1,
      modalVisible: false,
    };

    this.calulatorAmount = this.calulatorAmount.bind(this);
    this.onPressDelete = this.onPressDelete.bind(this);
    this.onPressBack = this.onPressBack.bind(this);
    this.onPressNext = this.onPressNext.bind(this);
    this.onPressSelect = this.onPressSelect.bind(this);
    this.onPressEdit = this.onPressEdit.bind(this);
    this.onActiveSwitch = this.onActiveSwitch.bind(this);
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  componentWillMount() {
    this.calulatorAmount();
  }

  onPressDelete() {
    this.props.onPressDelete();
  }

  onPressBack() {
    this.props.onPressBack(this.state);
  }

  onPressNext() {
    this.props.onPressNext(this.state);
  }

  onPressSelect(selected) {
    this.props.onPressSelect(selected.value);
  }

  onPressEdit() {
    this.props.onPressEdit(this.state);
  }

  calulatorAmount() {
    let amount = this.state.charityYearlyContribution * this.state.charityYears;
    this.setState({amount});
  }

  onActiveSwitch(val) {
    if (val === 1) {
      selected = this.state.charities[0];
    } else {
      selected = this.state.charities[1];
    }

    this.setState({
      activeSwitch: val,
      selected: selected
    });
  }

  render() {
    let amount = (this.state.amount * 1).toLocaleString('en');
    let title = this.props.title;

    return (
      <View style={{flex: 1}}>
        <ScrollView>
          <UiView styleName='horizontal space-between v-center' style={{paddingVertical: 10, paddingLeft: 15}}>
            <View style={{flexDirection: 'row'}}>
              <Heading styleName='h-center'>{title}</Heading>
              <TouchableHighlight onPress={() => {this.setModalVisible(true);}}>
                <Icon name="missing" />
              </TouchableHighlight>
            </View>

            {!this.state.customer &&
              <UiButton onPress={this.onPressDelete}>
                <Icon name="close" />
              </UiButton>
            }
          </UiView>

          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <SwitchButton
                onValueChange={this.onActiveSwitch}      // this is necessary for this component
                text1 = 'Yearly'                        // optional: first text in switch button --- default ON
                text2 = 'Lump-sum'                       // optional: second text in switch button --- default OFF
                switchWidth = {190}                 // optional: switch width --- default 44
                switchHeight = {40}                 // optional: switch height --- default 100
                switchdirection = 'rtl'             // optional: switch button direction ( ltr and rtl ) --- default ltr
                switchBorderRadius = {50}          // optional: switch border radius --- default oval
                switchSpeedChange = {200}           // optional: button change speed --- default 100
                switchBorderColor = '#d4d4d4'       // optional: switch border color --- default #d4d4d4
                switchBackgroundColor = '#fff'      // optional: switch background color --- default #fff
                btnBorderColor = '#00a4b9'          // optional: button border color --- default #00a4b9
                btnBackgroundColor = '#1980b9'      // optional: button background color --- default #00bcd4
                fontColor = '#b1b1b1'               // optional: text font color --- default #b1b1b1
                activeFontColor = '#fff'            // optional: active font color --- default #fff
                />
          </View>

          <FormGroup>
            <Caption styleName='bold' style={{fontSize: 20, lineHeight: 26}}>CONTRIBUTION</Caption>
            <TextInput
              style={{marginHorizontal: 15, marginVertical: 9, height: 45}}
              keyboardType={'numeric'}
              defaultValue={this.state.charityYearlyContribution}
              onChangeText={(charityYearlyContribution) => this.setState({charityYearlyContribution}, this.calulatorAmount)}
            />
          </FormGroup>
          <Divider styleName="line" />

          <FormGroup>
            <Caption styleName='bold' style={{fontSize: 20, lineHeight: 26}}>NUMBER OF YEARS</Caption>
            <TextInput
              style={{marginHorizontal: 15, marginVertical: 9, height: 45}}
              keyboardType={'numeric'}
              defaultValue={this.state.charityYears}
              onChangeText={(charityYears) => this.setState({charityYears}, this.calulatorAmount)}
            />
          </FormGroup>
          <Divider styleName="line" />

          <Title styleName='bold' style={{paddingTop: 10, paddingHorizontal: 15}}>TOTAL CONTRIBUTION</Title>
          <Title styleName='bold' style={{paddingVertical: 10, paddingHorizontal: 15}}>RM {amount}</Title>
        </ScrollView>

        {!this.state.customer &&
          <NavMenu
            onPressBack={this.onPressBack}
            onPressNext={this.onPressNext}
            onPressSelect={this.onPressSelect}
            />
        }

        {this.state.customer &&
          <UiButton
            style={{backgroundColor: '#1980b9', padding: 10}}
            onPress={this.onPressEdit}>
            <Title style={{color: 'white'}}>Submit</Title>
          </UiButton>
        }

        <View style={{marginTop: 22}}>
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}>
            <Screen style={{backgroundColor: '#fff'}}>
              <NavigationBar
                title="Information"
                styleName="inline"
                rightComponent={(
                  <Button onPress={() => {this.setModalVisible(!this.state.modalVisible);}}>
                    <Icon name="close" />
                  </Button>
                )}
              />

              <View style={{padding: 10}}>
                <Text>Charity fund is a fund dedicated to organization for them to carry out the goodwill of the deceased. This fund is reserved to ensure the good act of a person can be carry on even he is not around. For example, local underprivileged school, temple, orphanage, old folk’s home, and disability home.</Text>
              </View>
            </Screen>
          </Modal>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({

});
