import React, { Component } from 'react';
import { Modal, TouchableHighlight, View, Text, TextInput, StyleSheet, ScrollView } from 'react-native';

import { Screen, NavigationBar, Button, FormGroup, Caption, Divider,
  Heading, Title, Button as UiButton, Icon, DropDownMenu, DropDownModal,
  View as UiView, Text as UiText } from '@shoutem/ui';

import _ from 'lodash';
import { NavMenu } from '../helpers/NavMenu';

export class DebtForm extends Component {

  constructor(props) {
    super(props);

    let debts = [
      { title: 'Mortgages', value: 'House' },
      { title: 'Car Loan', value: 'Car' },
      { title: 'Personal Loan', value: 'Personal' },
      { title: 'Credit Card', value: 'CreditCard' },
      { title: 'Business Loan', value: 'Business' },
      { title: 'Others', value: 'Others' }
    ];

    let debtType = '';
    let debt = '1000';
    let selected = debts[0];
    // Maping record customer
    if (typeof props.customer != 'undefined' && props.customer.id) {
      let selectedIndex = _.findIndex(debts, { value: props.customer.selected.value });
      selected = debts[selectedIndex];
      debtType = props.customer.debtType;
      debt = props.customer.debt;
    }

    this.state = {
      amount: 0,
      // Debt Fees
      debtType: debtType,
      debt: debt,

      // Selected
      debts: debts,
      selected: selected,
      customer: props.customer,

      modalVisible: false,
    };

    this.calulatorAmount = this.calulatorAmount.bind(this);
    this.onPressDelete = this.onPressDelete.bind(this);
    this.onPressBack = this.onPressBack.bind(this);
    this.onPressNext = this.onPressNext.bind(this);
    this.onPressSelect = this.onPressSelect.bind(this);
    this.onPressEdit = this.onPressEdit.bind(this);
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  componentWillMount() {
    this.calulatorAmount();
  }

  onPressDelete() {
    this.props.onPressDelete();
  }

  onPressBack() {
    this.props.onPressBack(this.state);
  }

  onPressNext() {
    this.props.onPressNext(this.state);
  }

  onPressSelect(selected) {
    this.props.onPressSelect(selected.value);
  }

  onPressEdit() {
    this.props.onPressEdit(this.state);
  }

  calulatorAmount() {
    let amount = this.state.debt;
    this.setState({amount});
  }

  render() {
    let amount = (this.state.amount * 1).toLocaleString('en');
    let title = this.props.title;

    return (
      <View style={{flex: 1}}>
        <ScrollView>
          <UiView styleName='horizontal space-between v-center' style={{paddingVertical: 10, paddingLeft: 15}}>
            <View style={{flexDirection: 'row'}}>
              <Heading styleName='h-center'>{title}</Heading>
              <TouchableHighlight onPress={() => {this.setModalVisible(true);}}>
                <Icon name="missing" />
              </TouchableHighlight>
            </View>

            {!this.state.customer &&
              <UiButton onPress={this.onPressDelete}>
                <Icon name="close" />
              </UiButton>
            }
          </UiView>

          <DropDownMenu
            styleName="horizontal"
            options={this.state.debts}
            selectedOption={this.state.selected}
            onOptionSelected={option => this.setState({ selected: option })}
            titleProperty={"title"}
            valueProperty={"value"}
          />

          <FormGroup>
            <Caption styleName='bold' style={{fontSize: 20, lineHeight: 26}}>Total amount</Caption>
            <TextInput
              style={{marginHorizontal: 15, marginVertical: 9, height: 45}}
              keyboardType={'numeric'}
              defaultValue={this.state.debt}
              onChangeText={(debt) => this.setState({debt}, this.calulatorAmount)}
            />
          </FormGroup>
          <Divider styleName="line" />
        </ScrollView>

        {!this.state.customer &&
          <NavMenu
            onPressBack={this.onPressBack}
            onPressNext={this.onPressNext}
            onPressSelect={this.onPressSelect}
            />
        }

        {this.state.customer &&
          <UiButton
            style={{backgroundColor: '#1980b9', padding: 10}}
            onPress={this.onPressEdit}>
            <Title style={{color: 'white'}}>Submit</Title>
          </UiButton>
        }

        <View style={{marginTop: 22}}>
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}>
            <Screen style={{backgroundColor: '#fff'}}>
              <NavigationBar
                title="Information"
                styleName="inline"
                rightComponent={(
                  <Button onPress={() => {this.setModalVisible(!this.state.modalVisible);}}>
                    <Icon name="close" />
                  </Button>
                )}
              />

              <View style={{padding: 10}}>
                <Text>Debts are outstanding loan of a person make while he is alive. For example, mortgage, car loan, personal loan, business loan, credit card, and might be as guarantor.</Text>
              </View>
            </Screen>
          </Modal>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({

});
