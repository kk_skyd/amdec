import React, { Component } from 'react';
import { Modal, TouchableHighlight, View, Text, TextInput, StyleSheet, ScrollView } from 'react-native';

import { Screen, NavigationBar, Button, FormGroup, Caption, Divider,
  Heading, Title, Button as UiButton, Icon, DropDownMenu, DropDownModal,
  View as UiView, Text as UiText } from '@shoutem/ui';

import _ from 'lodash';
import { NavMenu } from '../helpers/NavMenu';

export class LegacyForm extends Component {

  constructor(props) {
    super(props);

    let legacy = '2000000';
    // Maping record customer
    if (typeof props.customer != 'undefined' && props.customer.id) {
      legacy = props.customer.legacy;
    }

    this.state = {
      amount: 0,
      // Legacy Fees
      legacy: legacy,
      customer: props.customer,

      modalVisible: false,
    };

    this.calulatorAmount = this.calulatorAmount.bind(this);
    this.onPressDelete = this.onPressDelete.bind(this);
    this.onPressBack = this.onPressBack.bind(this);
    this.onPressNext = this.onPressNext.bind(this);
    this.onPressSelect = this.onPressSelect.bind(this);
    this.onPressEdit = this.onPressEdit.bind(this);
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  componentWillMount() {
    this.calulatorAmount();
  }

  onPressDelete() {
    this.props.onPressDelete();
  }

  onPressBack() {
    this.props.onPressBack(this.state);
  }

  onPressNext() {
    this.props.onPressNext(this.state);
  }

  onPressSelect(selected) {
    this.props.onPressSelect(selected.value);
  }

  onPressEdit() {
    this.props.onPressEdit(this.state);
  }

  calulatorAmount() {
    let amount = this.state.legacy;
    this.setState({amount});
  }

  render() {
    let amount = (this.state.amount * 1).toLocaleString('en');
    let title = this.props.title;

    return (
      <View style={{flex: 1}}>
        <ScrollView>
          <UiView styleName='horizontal space-between v-center' style={{paddingVertical: 10, paddingLeft: 15}}>
            <View style={{flexDirection: 'row'}}>
              <Heading styleName='h-center'>{title}</Heading>
              <TouchableHighlight onPress={() => {this.setModalVisible(true);}}>
                <Icon name="missing" />
              </TouchableHighlight>
            </View>

            {!this.state.customer &&
              <UiButton onPress={this.onPressDelete}>
                <Icon name="close" />
              </UiButton>
            }
          </UiView>

          <FormGroup>
            <Caption styleName='bold' style={{fontSize: 20, lineHeight: 26}}>LEGACY AMOUNT</Caption>
            <TextInput
              style={{marginHorizontal: 15, marginVertical: 9, height: 45}}
              keyboardType={'numeric'}
              defaultValue={this.state.legacy}
              onChangeText={(legacy) => this.setState({legacy}, this.calulatorAmount)}
            />
          </FormGroup>
          <Divider styleName="line" />
        </ScrollView>

        {!this.state.customer &&
          <NavMenu
            onPressBack={this.onPressBack}
            onPressNext={this.onPressNext}
            onPressSelect={this.onPressSelect}
            />
        }

        {this.state.customer &&
          <UiButton
            style={{backgroundColor: '#1980b9', padding: 10}}
            onPress={this.onPressEdit}>
            <Title style={{color: 'white'}}>Submit</Title>
          </UiButton>
        }

        <View style={{marginTop: 22}}>
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}>
            <Screen style={{backgroundColor: '#fff'}}>
              <NavigationBar
                title="Information"
                styleName="inline"
                rightComponent={(
                  <Button onPress={() => {this.setModalVisible(!this.state.modalVisible);}}>
                    <Icon name="close" />
                  </Button>
                )}
              />

              <View style={{padding: 10}}>
                <Text>Legacy is a fund created by a person for his/her descendants. this legacy fund can be used by them for their business kick start, marriage, buy their first house or fund to help them fulfill their dreams.</Text>
              </View>
            </Screen>
          </Modal>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({

});
