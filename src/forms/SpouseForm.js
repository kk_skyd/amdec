import React, { Component } from 'react';
import { Modal, TouchableHighlight, View, Text, TextInput, StyleSheet, ScrollView } from 'react-native';

import { Screen, NavigationBar, Button, FormGroup, Caption, Divider,
  Heading, Title, Button as UiButton, Icon, DropDownMenu, DropDownModal,
  View as UiView, Text as UiText } from '@shoutem/ui';

import _ from 'lodash';
import { NavMenu } from '../helpers/NavMenu';

export class SpouseForm extends Component {

  constructor(props) {
    super(props);

    let types = [
      { title: 'Children', value: 'children' },
      { title: 'Dad', value: 'dad' },
      { title: 'Mom', value: 'mom' },
      { title: 'Spouse', value: 'spouse' }
    ];
    let selected = types[0];

    let monthly = '1000';
    let yearly = '12000';
    let age = '30';
    let ageUtil = '50';
    // Maping record customer
    if (typeof props.customer != 'undefined' && props.customer.id) {
      monthly = props.customer.monthly;
      yearly = props.customer.yearly;
      age = props.customer.age;
      ageUtil = props.customer.ageUtil;

      let selectedIndex = _.findIndex(types, { value: props.customer.selected.value });
      selected = types[selectedIndex];
    }

    this.state = {
      amount: 0,
      // Maintenance Fees
      monthly: monthly,
      yearly: yearly,
      age: age,
      ageUtil: ageUtil,

      // Selected
      types: types,
      selected: selected,
      customer: props.customer,

      modalVisible: false,
    };

    this.calulatorAmount = this.calulatorAmount.bind(this);
    this.onPressDelete = this.onPressDelete.bind(this);
    this.onPressBack = this.onPressBack.bind(this);
    this.onPressNext = this.onPressNext.bind(this);
    this.onPressSelect = this.onPressSelect.bind(this);
    this.onPressEdit = this.onPressEdit.bind(this);
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  componentWillMount() {
    this.calulatorAmount();
  }

  onPressDelete() {
    this.props.onPressDelete();
  }

  onPressBack() {
    this.props.onPressBack(this.state);
  }

  onPressNext() {
    this.props.onPressNext(this.state);
  }

  onPressSelect(selected) {
    this.props.onPressSelect(selected.value);
  }

  onPressEdit() {
    this.props.onPressEdit(this.state);
  }

  calulatorAmount() {
    let amount = (this.state.ageUtil - this.state.age) * this.state.monthly * 12;
    this.setState({amount});
  }

  render() {
    let amount = (this.state.amount * 1).toLocaleString('en');
    let title = this.props.title;
    let yearly = (this.state.monthly * 12).toLocaleString('en');

    return (
      <View style={{flex: 1}}>
        <ScrollView>
          <UiView styleName='horizontal space-between v-center' style={{paddingVertical: 10, paddingLeft: 15}}>
            <View style={{flexDirection: 'row'}}>
              <Heading styleName='h-center'>{title}</Heading>
              <TouchableHighlight onPress={() => {this.setModalVisible(true);}}>
                <Icon name="missing" />
              </TouchableHighlight>
            </View>

            {!this.state.customer &&
              <UiButton onPress={this.onPressDelete}>
                <Icon name="close" />
              </UiButton>
            }
          </UiView>

          {this.state.customer &&
            <DropDownMenu
              styleName="horizontal"
              options={this.state.types}
              selectedOption={this.state.selected}
              onOptionSelected={option => this.setState({ selected: option })}
              titleProperty={"title"}
              valueProperty={"value"}
            />
          }

          <FormGroup>
            <Caption styleName='bold' style={{fontSize: 20, lineHeight: 26}}>MONTHLY LIVING EXPENSE</Caption>
            <TextInput
              style={{marginHorizontal: 15, marginVertical: 9, height: 45}}
              keyboardType={'numeric'}
              defaultValue={this.state.monthly}
              onChangeText={(monthly) => this.setState({monthly}, this.calulatorAmount)}
            />
          </FormGroup>
          <Divider styleName="line" />

          <FormGroup>
            <Caption styleName='bold' style={{fontSize: 20, lineHeight: 26}}>YEARLY LIVING EXPENSE</Caption>
            <TextInput
              style={{marginHorizontal: 15, marginVertical: 9, height: 45}}
              editable={false}
              keyboardType={'numeric'}
              defaultValue={yearly}
              onChangeText={(yearly) => this.setState({yearly}, this.calulatorAmount)}
            />
          </FormGroup>
          <Divider styleName="line" />

          <FormGroup>
            <Caption styleName='bold' style={{fontSize: 20, lineHeight: 26}}>AGE</Caption>
            <TextInput
              style={{marginHorizontal: 15, marginVertical: 9, height: 45}}
              keyboardType={'numeric'}
              defaultValue={this.state.age}
              onChangeText={(age) => this.setState({age}, this.calulatorAmount)}
            />
          </FormGroup>
          <Divider styleName="line" />

          <FormGroup>
            <Caption styleName='bold' style={{fontSize: 20, lineHeight: 26}}>SUPPORTS UNTIL AGE OF</Caption>
            <TextInput
              style={{marginHorizontal: 15, marginVertical: 9, height: 45}}
              keyboardType={'numeric'}
              defaultValue={this.state.ageUtil}
              onChangeText={(ageUtil) => this.setState({ageUtil}, this.calulatorAmount)}
            />
          </FormGroup>
          <Divider styleName="line" />

          <Title styleName='bold' style={{paddingTop: 10, paddingHorizontal: 15}}>TOTAL ALLOCATION</Title>
          <Title styleName='bold' style={{paddingVertical: 10, paddingHorizontal: 15}}>RM {amount}</Title>
        </ScrollView>

        {!this.state.customer &&
          <NavMenu
            onPressBack={this.onPressBack}
            onPressNext={this.onPressNext}
            onPressSelect={this.onPressSelect}
            />
        }

        {this.state.customer &&
          <UiButton
            style={{backgroundColor: '#1980b9', padding: 10}}
            onPress={this.onPressEdit}>
            <Title style={{color: 'white'}}>Submit</Title>
          </UiButton>
        }

        <View style={{marginTop: 22}}>
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}>
            <Screen style={{backgroundColor: '#fff'}}>
              <NavigationBar
                title="Information"
                styleName="inline"
                rightComponent={(
                  <Button onPress={() => {this.setModalVisible(!this.state.modalVisible);}}>
                    <Icon name="close" />
                  </Button>
                )}
              />

              <View style={{padding: 10}}>
                <Text>Maintenance costs are the cost that required to maintain the quality of living standard of family. Those expenses include monthly living expense, child’s tuition fee medical fees, accommodation,  utilities, transportation fees, and guardianship. It is advisable to support monthly expense of the child until age of 25.</Text>
              </View>
            </Screen>
          </Modal>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({

});
