import React, { Component } from 'react';
import { Modal, TouchableHighlight, View, Text, TextInput, StyleSheet, ScrollView } from 'react-native';

import { Screen, NavigationBar, Button, FormGroup, Caption, Divider,
  Heading, Title, Button as UiButton, Icon, DropDownMenu, DropDownModal,
  View as UiView, Text as UiText } from '@shoutem/ui';

import { NavMenu } from '../helpers/NavMenu';

export class AdministrationCostForm extends Component {

  constructor(props) {
    super(props);

    let administrationEstateAmount = '2000000';
    // Maping record customer
    if (typeof props.customer != 'undefined' && props.customer.id) {
      administrationEstateAmount = props.customer.administrationEstateAmount;
    }

    this.state = {
      amount: 0,
      // Administration Cost
      administrationEstateAmount: administrationEstateAmount,
      customer: props.customer,

      modalVisible: false,
    };

    this.calulatorAmount = this.calulatorAmount.bind(this);
    this.onPressDelete = this.onPressDelete.bind(this);
    this.onPressBack = this.onPressBack.bind(this);
    this.onPressNext = this.onPressNext.bind(this);
    this.onPressSelect = this.onPressSelect.bind(this);
    this.onPressEdit = this.onPressEdit.bind(this);
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  componentWillMount() {
    this.calulatorAmount();
  }

  onPressDelete() {
    this.props.onPressDelete();
  }

  onPressBack() {
    this.props.onPressBack(this.state);
  }

  onPressNext() {
    this.props.onPressNext(this.state);
  }

  onPressSelect(selected) {
    this.props.onPressSelect(selected.value);
  }

  onPressEdit() {
    this.props.onPressEdit(this.state);
  }

  calulatorAmount() {
    let amount = amount = this.state.administrationEstateAmount * 10 / 100;
    this.setState({amount});
  }

  render() {
    let amount = (this.state.amount * 1).toLocaleString('en');
    let title = this.props.title;

    return (
      <View style={{flex: 1}}>
        <ScrollView>
          <UiView styleName='horizontal space-between v-center' style={{paddingVertical: 10, paddingLeft: 15}}>
            <View style={{flexDirection: 'row'}}>
              <Heading styleName='h-center'>{title}</Heading>
              <TouchableHighlight onPress={() => {this.setModalVisible(true);}}>
                <Icon name="missing" />
              </TouchableHighlight>
            </View>

            {!this.state.customer &&
              <UiButton onPress={this.onPressDelete}>
                <Icon name="close" />
              </UiButton>
            }
          </UiView>

          <FormGroup>
            <Caption styleName='bold' style={{fontSize: 20, lineHeight: 26}}>ESTATE AMOUNT</Caption>
            <TextInput
              style={{marginHorizontal: 15, marginVertical: 9, height: 45}}
              keyboardType={'numeric'}
              defaultValue={this.state.administrationEstateAmount}
              onChangeText={(administrationEstateAmount) => this.setState({administrationEstateAmount}, this.calulatorAmount)}
            />
          </FormGroup>
          <Divider styleName="line" />

          <Title styleName='bold' style={{paddingTop: 10, paddingHorizontal: 15}}>TOTAL ADMINISTRATION COST</Title>
          <Title styleName='bold' style={{paddingVertical: 10, paddingHorizontal: 15}}>RM {amount}</Title>
        </ScrollView>

        {!this.state.customer &&
          <NavMenu
            onPressBack={this.onPressBack}
            onPressNext={this.onPressNext}
            onPressSelect={this.onPressSelect}
            />
        }

        {this.state.customer &&
          <UiButton
            style={{backgroundColor: '#1980b9', padding: 10}}
            onPress={this.onPressEdit}>
            <Title style={{color: 'white'}}>Submit</Title>
          </UiButton>
        }

        <View style={{marginTop: 22}}>
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}>
            <Screen style={{backgroundColor: '#fff'}}>
              <NavigationBar
                title="Information"
                styleName="inline"
                rightComponent={(
                  <Button onPress={() => {this.setModalVisible(!this.state.modalVisible);}}>
                    <Icon name="close" />
                  </Button>
                )}
              />

              <View style={{padding: 10}}>
                <Text>Administration costs are  fees that may imposed on family after a persons death such as taxes, legal fees, administrator and broker fees, stamp duty, estate duty, funeral expenses, others hidden cost. It is estimated about 10% of the estate amount.</Text>
              </View>
            </Screen>
          </Modal>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({

});
