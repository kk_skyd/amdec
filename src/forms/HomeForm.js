import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';

import { Image, FormGroup,
  Caption, TextInput,
  Divider, DropDownMenu,
  Button as UiButton, Icon,
  View as UiView, Text as UiText } from '@shoutem/ui';

import { RkChoiceGroup, RkChoice } from 'react-native-ui-kitten';
import DatePicker from 'react-native-datepicker';

export class HomeForm extends Component {

  constructor(props) {
    super(props);

    let childrens = [
      { title: 'NIL', value: '0' },
      { title: '1 children', value: '1' },
      { title: '2 children', value: '2' },
      { title: '3 children', value: '3' },
      { title: '4 children', value: '4' },
      { title: '5 children', value: '5' },
      { title: '6 children', value: '6' },
      { title: '7 children', value: '7' },
      { title: '8 children', value: '8' },
      { title: '9 children', value: '9' },
      { title: '10 children', value: '10' }
    ];

    this.state = {
      name: '',
      age: '18',
      dob: '2000-01-01',
      hp: '',
      male: true,
      female: false,
      contact: '',
      dad: true,
      mom: true,
      spouse: true,
      childrens: childrens,
      selectedChild: childrens[0]
    };

    this.onPressNext = this.onPressNext.bind(this);
    this.onPressChilds = this.onPressChilds.bind(this);
    this.onPressDOB = this.onPressDOB.bind(this);
  }

  onPressDOB(date) {
    let from = date.split("-");
    let age = (new Date()).getFullYear() - from[0];
    this.setState({dob: date, age: age});
  }

  onPressNext() {
    this.props.onSubmit(this.state);
  }

  onPressChilds() {
    this.refs.childs.collapse;
  }

  render() {
    let childrens = [[1, 2, 3, 4, 5]];
    return (
      <View style={{flex: 1}}>
        <ScrollView>
          <FormGroup>
            <Caption styleName='bold' style={{fontSize: 20, lineHeight: 26}}>Name</Caption>
            <TextInput
              style={{height: 45}}
              defaultValue={this.state.name}
              onChangeText={(name) => this.setState({name})}
            />
          </FormGroup>
          <Divider styleName="line" />

          <FormGroup>
            <Caption styleName='bold' style={{fontSize: 20, lineHeight: 26}}>Date of birth</Caption>
            <DatePicker
              style={{width: 150, paddingVertical: 9, paddingHorizontal: 15}}
              date={this.state.dob}
              mode="date"
              placeholder="select date"
              format="YYYY-MM-DD"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
                dateInput: {
                  flex: 1, height: 35, borderWidth: 0
                }
              }}
              onDateChange={this.onPressDOB}
            />
          </FormGroup>
          <Divider styleName="line" />

          <FormGroup>
            <Caption styleName='bold' style={{fontSize: 20, lineHeight: 26}}>H/P</Caption>
            <TextInput
              style={{height: 45}}
              defaultValue={this.state.hp}
              onChangeText={(hp) => this.setState({hp})}
            />
          </FormGroup>
          <Divider styleName="line" />

          <UiView styleName='horizontal h-center' style={{marginVertical: 10}}>
            <RkChoiceGroup radio={true} onChange={() => this.setState({male: true, female: false})}>
              <TouchableOpacity choiceTrigger>
                <View style={{flex: 1, flexDirection:'row', alignItems:'center', marginRight:20}}>
                  <RkChoice rkType='radio'
                    style={{marginBottom: 5, marginRight: 10}}
                    selected={this.state.male}
                    onPress={() => this.setState({male: !this.state.male})}
                  />
                  <Caption>Male</Caption>
                </View>
              </TouchableOpacity>
            </RkChoiceGroup>

            <RkChoiceGroup radio={true} onChange={() => this.setState({male: false, female: true})}>
              <TouchableOpacity choiceTrigger>
                <View style={{flex: 1, flexDirection:'row', alignItems:'center'}}>
                  <RkChoice rkType='radio'
                    style={{marginBottom: 5, marginRight: 10}}
                    selected={this.state.female}
                    onPress={() => this.setState({female: !this.state.female})}
                  />
                  <Caption>Female</Caption>
                </View>
              </TouchableOpacity>
            </RkChoiceGroup>
          </UiView>

          <FormGroup>
            <Caption styleName='bold' style={{fontSize: 20, lineHeight: 26}}>Contact Number</Caption>
            <TextInput
              style={{height: 45}}
              defaultValue={this.state.contact}
              onChangeText={(contact) => this.setState({contact})}
            />
          </FormGroup>
          <Divider styleName="line" />

          <UiView styleName='horizontal v-start' style={{paddingHorizontal: 15, paddingVertical: 10}}>
            <UiView styleName='vertical' style={{flex: 1, alignItems: 'center'}}>
              <Caption styleName='bold' style={{marginBottom: 10}}>Parents</Caption>

              <UiView styleName='horizontal'>
                <UiView styleName='vertical'>
                  <TouchableOpacity onPress={() => this.setState({dad: !this.state.dad})}>
                    <Image
                     resizeMode="stretch"
                     style={{ width: 64, height: 100 }}
                     source={require('../assets/images/dad.png')} />
                  </TouchableOpacity>

                  <View style={{alignItems: 'center', justifyContent: 'center', height: 28}}>
                    {this.state.dad &&
                      <TouchableOpacity onPress={() => this.setState({dad: !this.state.dad})}>
                        <Image
                         resizeMode="stretch"
                         style={{ margin: 4, width: 20, height: 20, tintColor: '#22c92d' }}
                         source={require('../assets/images/checkMark.png')} />
                      </TouchableOpacity>
                    }
                  </View>
                </UiView>

                <UiView styleName='vertical'>
                  <TouchableOpacity onPress={() => this.setState({mom: !this.state.mom})}>
                    <Image
                     resizeMode="stretch"
                     style={{ width: 62, height: 100 }}
                     source={require('../assets/images/mom.png')} />
                  </TouchableOpacity>

                  <View style={{alignItems: 'center', justifyContent: 'center'}}>
                    {this.state.mom &&
                      <TouchableOpacity onPress={() => this.setState({mom: !this.state.mom})}>
                        <Image
                         resizeMode="stretch"
                         style={{ margin: 4, width: 20, height: 20, tintColor: '#22c92d' }}
                         source={require('../assets/images/checkMark.png')} />
                      </TouchableOpacity>
                    }
                  </View>
                </UiView>
              </UiView>
            </UiView>

            <UiView styleName='vertical' style={{flex: 1, alignItems: 'center', paddingHorizontal: 10}}>
              <Caption styleName='bold' style={{marginBottom: 10}}>Spouse</Caption>

              <TouchableOpacity onPress={() => this.setState({spouse: !this.state.spouse})}>
                <Image
                 resizeMode="stretch"
                 style={{ width: 134, height: 100 }}
                 source={require('../assets/images/spouse.png')} />
              </TouchableOpacity>

              <View style={{alignItems: 'center', justifyContent: 'center'}}>
                {this.state.spouse &&
                  <TouchableOpacity onPress={() => this.setState({spouse: !this.state.spouse})}>
                    <Image
                     resizeMode="stretch"
                     style={{ margin: 4, width: 20, height: 20, tintColor: '#22c92d' }}
                     source={require('../assets/images/checkMark.png')} />
                  </TouchableOpacity>
                }
             </View>
            </UiView>

            <View style={{flex:1, flexDirection:'column', alignItems: 'center'}}>
              <Caption styleName='bold' style={{marginBottom: 10}}>Children</Caption>

              <View style={{alignItems: 'center'}}>
                <Image
                  resizeMode="stretch"
                  style={{ width: 72, height: 100 }}
                  source={require('../assets/images/childs.png')} />

                <DropDownMenu ref="childs"
                  options={this.state.childrens}
                  selectedOption={this.state.selectedChild}
                  onOptionSelected={(child) => this.setState({ selectedChild: child })}
                  titleProperty="title"
                  valueProperty="value"
                />
              </View>
            </View>
          </UiView>
        </ScrollView>

        <UiView styleName='horizontal h-end' style={{paddingVertical: 10, paddingHorizontal: 10}}>
          <UiView style={{borderBottomWidth: 1, borderColor: 'gray'}}>
            <UiButton onPress={this.onPressNext}>
              <UiText>NEXT</UiText>
              <Icon name="right-arrow" />
            </UiButton>
          </UiView>
        </UiView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  input: {
    borderBottomColor: '#000000',
    borderBottomWidth: 1
  }
});
