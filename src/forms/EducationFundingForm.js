import React, { Component } from 'react';
import { Dimensions, Image, Modal, TouchableHighlight, View, Text, TextInput, StyleSheet, ScrollView } from 'react-native';

import { Screen, NavigationBar, Button, FormGroup, Caption, Divider,
  Heading, Title, Button as UiButton, Icon, DropDownMenu, DropDownModal,
  View as UiView, Text as UiText } from '@shoutem/ui';

import { NavMenu } from '../helpers/NavMenu';

export class EducationFundingForm extends Component {

  constructor(props) {
    super(props);

    let educationChildrens = this.props.childrens;
    let educationFees = '200000';
    // Maping record customer
    if (typeof props.customer != 'undefined' && props.customer.id) {
      educationChildrens = props.customer.educationChildrens;
      educationFees = props.customer.educationFees;
    }

    this.state = {
      amount: 0,
      // Educational Funding
      educationChildrens: educationChildrens,
      educationFees: educationFees,
      customer: props.customer,

      modalVisible: false,
    };

    this.calulatorAmount = this.calulatorAmount.bind(this);
    this.onPressDelete = this.onPressDelete.bind(this);
    this.onPressBack = this.onPressBack.bind(this);
    this.onPressNext = this.onPressNext.bind(this);
    this.onPressSelect = this.onPressSelect.bind(this);
    this.onPressEdit = this.onPressEdit.bind(this);
  }

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  componentWillMount() {
    this.calulatorAmount();
  }

  onPressDelete() {
    this.props.onPressDelete();
  }

  onPressBack() {
    this.props.onPressBack(this.state);
  }

  onPressNext() {
    this.props.onPressNext(this.state);
  }

  onPressSelect(selected) {
    this.props.onPressSelect(selected.value);
  }

  onPressEdit() {
    this.props.onPressEdit(this.state);
  }

  calulatorAmount() {
    let amount = this.state.educationChildrens * this.state.educationFees;
    this.setState({amount});
  }

  render() {
    const win = Dimensions.get('window');
    const ratio = win.width/1236;
    let imageStyle = {
      width: win.width,
      height: 876 * ratio, //362 is actual height of image
    }

    let amount = (this.state.amount * 1).toLocaleString('en');
    let title = this.props.title;

    return (
      <View style={{flex: 1}}>
        <ScrollView>
          <UiView styleName='horizontal space-between v-center' style={{paddingVertical: 10, paddingLeft: 15}}>
            <View style={{flexDirection: 'row'}}>
              <Heading styleName='h-center'>{title}</Heading>
              <TouchableHighlight onPress={() => {this.setModalVisible(true);}}>
                <Icon name="missing" />
              </TouchableHighlight>
            </View>

            {!this.state.customer &&
              <UiButton onPress={this.onPressDelete}>
                <Icon name="close" />
              </UiButton>
            }
          </UiView>

          <FormGroup>
            <Caption styleName='bold' style={{fontSize: 20, lineHeight: 26}}>NUMBER OF CHILDREN</Caption>
            <TextInput
              style={{marginHorizontal: 15, marginVertical: 9, height: 45}}
              keyboardType={'numeric'}
              defaultValue={this.state.educationChildrens}
              onChangeText={(educationChildrens) => this.setState({educationChildrens}, this.calulatorAmount)}
            />
          </FormGroup>
          <Divider styleName="line" />

          <FormGroup>
            <Caption styleName='bold' style={{fontSize: 20, lineHeight: 26}}>ESTIMATED EDUCATION FEES/PERSON</Caption>
            <TextInput
              style={{marginHorizontal: 15, marginVertical: 9, height: 45}}
              keyboardType={'numeric'}
              defaultValue={this.state.educationFees}
              onChangeText={(educationFees) => this.setState({educationFees}, this.calulatorAmount)}
            />
          </FormGroup>
          <Divider styleName="line" />

          <Title styleName='bold' style={{paddingTop: 10, paddingHorizontal: 15}}>TOTAL EDUCATION FUND</Title>
          <Title styleName='bold' style={{paddingVertical: 10, paddingHorizontal: 15}}>RM {amount}</Title>
        </ScrollView>

        {!this.state.customer &&
          <NavMenu
            onPressBack={this.onPressBack}
            onPressNext={this.onPressNext}
            onPressSelect={this.onPressSelect}
            />
        }

        {this.state.customer &&
          <UiButton
            style={{backgroundColor: '#1980b9', padding: 10}}
            onPress={this.onPressEdit}>
            <Title style={{color: 'white'}}>Submit</Title>
          </UiButton>
        }

        <View style={{marginTop: 22}}>
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}>
            <Screen style={{backgroundColor: '#fff'}}>
              <NavigationBar
                title="Information"
                styleName="inline"
                rightComponent={(
                  <Button onPress={() => {this.setModalVisible(!this.state.modalVisible);}}>
                    <Icon name="close" />
                  </Button>
                )}
              />

              <View style={{padding: 10}}>
                <Text>Education funding is an act of providing money resources to finance a childs education.</Text>
              </View>
              <View>
                <Image
                  resizeMode={'cover'}
                  style={imageStyle}
                  source={require('../assets/info/education.png')} />
              </View>
            </Screen>
          </Modal>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({

});
