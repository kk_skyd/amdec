import React, { Component } from 'react';
import {
  AsyncStorage,
  StyleSheet,
  Text,
  TextInput,
  View,
  StatusBar,
  TouchableOpacity,
  KeyboardAvoidingView,
  Image
} from 'react-native';

import firebase from 'firebase';

export class LoginScreen extends Component {

  static navigationOptions = {
    header: null
  };

  constructor() {
    super();

    this.state = {
      loggedIn: false,
      email: '',
      password: '',
      error: ''
    };

    this.toLogin = this.toLogin.bind(this);
  }

  componentWillMount() {
    firebase.initializeApp({
      apiKey: "AIzaSyAlk8MhJk3WonDlYkx4fkFg-6TT_3mpLuY",
      authDomain: "agentproject-21ee4.firebaseapp.com",
      databaseURL: "https://agentproject-21ee4.firebaseio.com",
      projectId: "agentproject-21ee4",
      storageBucket: "agentproject-21ee4.appspot.com",
      messagingSenderId: "227013053462",
    });

    // firebase.auth().onAuthStateChanged((user) => {
    //   if (user) {
    //
    //     let uuid = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
    //     let userId = user.uid;
    //     var loginKey = firebase.database().ref('users/' + userId + '/key');
    //     const { navigate } = this.props.navigation;
    //
    //     loginKey.on('value', function(key) {
    //       let keyCheck = key.val();
    //
    //       if (keyCheck && keyCheck != this.state.loggedKey) {
    //         firebase.auth().signOut().then(() => {
    //           this.setState({ loggedIn: false, loggedKey: '' });
    //           navigate('Login');
    //         });
    //       } else {
    //         firebase.database().ref('users/' + userId).set({
    //           key: uuid
    //         });
    //
    //         this.setState({ loggedIn: true, loggedKey: uuid });
    //       }
    //     });
    //
    //   } else {
    //     this.setState({ loggedIn: false });
    //   }
    // });
  }

  componentDidMount() {
    AsyncStorage.getItem(
      '@CustomerListStore:User', (err, data) => {
        if (err) {
          console.error('Error loading customers', err);
        } else {
          if (data) {
            let user = JSON.parse(data);
            let userId = user.uid;
            let loginKey = firebase.database().ref('users/' + userId + '/key');
            const { navigate } = this.props.navigation;

            loginKey.on('value', function(key) {
              let keyCheck = key.val();
              if (keyCheck == user.key) {
                navigate('Home');
              } else {
                AsyncStorage.removeItem('@CustomerListStore:User');
                firebase.auth().signOut();
              }
            });
          }
        }
    });
  }

  toLogin() {
    const { navigate } = this.props.navigation;
    const { email, password } = this.state;

    this.setState({
      error: 'Wait for loading...'
    });

    firebase.auth().signInWithEmailAndPassword(email, password)
      .then((user) => {
        let keyNew = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        let userId = user.uid;
        let loginKey = firebase.database().ref('users/' + userId + '/key');
        const { navigate } = this.props.navigation;
        let self = this;

        loginKey.on('value', function(key) {
          let keyCheck = key.val();
          if (keyCheck == null) {
            AsyncStorage.setItem('@CustomerListStore:User', JSON.stringify({uid: userId, key: keyNew})).then(res => {
              self.setState({
                error: 'Login successful'
              }, () => {
                firebase.database().ref('users/' + userId).set({ key: keyNew });
                navigate('Home');
              });
            });
          } else {
            self.setState({
              error: 'The user already login'
            }, () => {
              firebase.auth().signOut();
            });
          }
        });

      })
      .catch((e) => {
        this.setState({
          error: '' + e
        });
      });
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.container}>
          <StatusBar hidden={true} />

          <View style={styles.logoConten}>
            <Image
             resizeMode="contain"
             style={styles.logo}
             source={require('../assets/images/logo.png')} />

          </View>

          <KeyboardAvoidingView style={styles.containerForm}>
            <Text style={styles.error}>{this.state.error}</Text>

            <TextInput
              placeholder = "Email"
              placeholderTextColor = '#cccccc'
              returnKeyType="next"
              keyboardType = "email-address"
              autoCorrect = {false}
              autoCapitalize = "none"
              onSubmitEditing = {()=> this.passwordInput.focus()}
              onChangeText={email => this.setState({ email })}
              style = {styles.input}>
            </TextInput>

            <TextInput
              placeholder ="Password"
              placeholderTextColor ='#cccccc'
              secureTextEntry
              returnKeyType="go"
              ref = {(input)=>this.passwordInput = input}
              onChangeText={password => this.setState({ password })}
              style = {styles.input}>
            </TextInput>

            <TouchableOpacity style={styles.buttonContainer}>
              <Text onPress={this.toLogin}
                style={styles.loginbutton}>
                CLICK TO LOGIN
              </Text>
            </TouchableOpacity>
          </KeyboardAvoidingView>
      </View>
    )
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: 'white',
  },
  logoConten: {
    height: 250,
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleApp: {
    width : 200,
    fontSize: 18,
    textAlign: 'center',
    margin: 10,
    color :'blue'
  },
  logo:{
    width: 300,
    // height: 100
  },
  containerForm: {
    padding :20,
    flex: 1
  },
  error: {
    marginBottom: 5,
    color: 'red'
  },
  input:{
    minWidth:300,
    flexWrap:'wrap',
    height : 40,
    backgroundColor: '#f2f2f2',
    paddingHorizontal : 10,
    color:'#595959',
    marginBottom : 10,
  },
  buttonContainer:{
    backgroundColor: "#1980b9",
    paddingVertical:10,
    marginTop:15,
    marginBottom:20
  },
  loginbutton:{
    color: '#ffffff',
    textAlign:'center',
    fontWeight:'700'
  }
});

export default LoginScreen;
