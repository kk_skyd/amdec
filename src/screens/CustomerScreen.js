import React, { Component } from 'react';
import { AsyncStorage, Alert, StyleSheet, ScrollView } from 'react-native';
import { Button, View, Screen, NavigationBar, Icon, Title } from '@shoutem/ui';

import { Customer } from '../components/Customer';

import _ from 'lodash';
import { SpouseForm } from '../forms/SpouseForm';
import { DebtForm } from '../forms/DebtForm';
import { EducationFundingForm } from '../forms/EducationFundingForm';
import { AdministrationCostForm } from '../forms/AdministrationCostForm';
import { CharityForm } from '../forms/CharityForm';
import { LegacyForm } from '../forms/LegacyForm';

export class CustomerScreen extends Component {

  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    const { state } = this.props.navigation;

    this.state = {
      customers: [],
      customer: state.params.customer
    };

    this.onPressDelete = this.onPressDelete.bind(this);
    this.goBack = this.goBack.bind(this);
    this.editCustomer = this.editCustomer.bind(this);
  }

  componentDidMount() {
    AsyncStorage.getItem(
      '@CustomerListStore:Customers', (err, data) => {
        if (err) {
          console.error('Error loading customers', err);
        } else {
          let customers = JSON.parse(data);
          if (Array.isArray(customers)) {
            this.setState({ customers });
          }
        }
    });
  }

  saveCustomers(customers) {
    AsyncStorage.setItem(
      '@CustomerListStore:Customers',
      JSON.stringify(customers)
    ).then(res => {
      const { navigate } = this.props.navigation;
      navigate('Summary');
    });
  }

  editCustomer(data) {
    let customers = this.state.customers;
    let customer = this.state.customer;
    let customerClone = {
      id: customer.id,
      profile: customer.profile,
      ... data,
      name: customer.name,
      home: customer.home
    };
    delete customerClone.customer;

    let index = _.findIndex(customers, { 'id': customer.id});
    customers[index] = customerClone;
    this.saveCustomers(customers);
  }

  removeCustomer() {
    let customers = this.state.customers;
    let id = this.state.customer.id;
    customers = customers.filter( (data) => data.id !== id );
    this.saveCustomers(customers);
  }

  onPressDelete() {
    Alert.alert(
      'Delete data!',
      'Are you sure?',
      [
        { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        { text: 'OK', onPress: () => this.removeCustomer() } ,
      ],
      { cancelable: false }
    );
  }

  goBack() {
    const { goBack } = this.props.navigation;
    goBack();
  }

  render() {
    let customer = this.state.customer;

    return (
      <View style={{backgroundColor: 'white', flex: 1}}>
        <View style={{height: 70}}>
          <NavigationBar
            leftComponent={(
              <Button onPress={this.goBack}>
                <Icon name="back" />
              </Button>
            )}
            centerComponent={<Title>Customer</Title>}
            rightComponent={(
              <Button onPress={this.onPressDelete}>
                <Icon name="close" />
              </Button>
            )}
          />
        </View>

        <ScrollView>
          {customer.profile == 'maintenance' &&
            <SpouseForm
              onPressEdit={(data) => this.editCustomer(data)}
              customer={customer} title='Maintenance' />
          }

          {customer.profile == 'debt' &&
            <DebtForm
              onPressEdit={(data) => this.editCustomer(data)}
              customer={customer} title='Debt' />
          }

          {customer.profile == 'education' &&
            <EducationFundingForm
              onPressEdit={(data) => this.editCustomer(data)}
              customer={customer} title='Education Funding' />
          }

          {customer.profile == 'administration' &&
            <AdministrationCostForm
              onPressEdit={(data) => this.editCustomer(data)}
              customer={customer} title='Administration Cost' />
          }

          {customer.profile == 'charity' &&
            <CharityForm
              onPressEdit={(data) => this.editCustomer(data)}
              customer={customer} title='Charity' />
          }

          {customer.profile == 'legacy' &&
            <LegacyForm
              onPressEdit={(data) => this.editCustomer(data)}
              customer={customer} title='Legacy' />
          }
        </ScrollView>
      </View>
    )
  }
}
