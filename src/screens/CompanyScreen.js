import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View, StatusBar} from 'react-native';
import { Button, NavigationBar, Icon, Title } from '@shoutem/ui';

export class CompanyScreen extends Component {

  static navigationOptions = {
    header: null
  };

  constructor() {
    super();
    this.onPressDrawer = this.onPressDrawer.bind(this);
  }

  onPressDrawer() {
    const { goBack } = this.props.navigation;
    goBack(null);
  }

  render() {
    return (
      <View style={{backgroundColor: 'white', flex: 1}}>
        <View style={{height: 70}}>
          <NavigationBar
            leftComponent={(<Button onPress={this.onPressDrawer}><Icon name="back" /></Button>)}
            centerComponent={<Title>Company</Title>}
          />
        </View>

        <ScrollView>
            <View>
              <Text>Update...</Text>
            </View>
        </ScrollView>
      </View>
    )
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default CompanyScreen;
