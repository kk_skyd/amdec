import React, { Component } from 'react';
import { AsyncStorage, Alert, StyleSheet, ScrollView } from 'react-native';
import { DropDownMenu, Button, View, Screen, NavigationBar, Icon, Title } from '@shoutem/ui';

import { Customer } from '../components/Customer';

import _ from 'lodash';
import { SpouseForm } from '../forms/SpouseForm';
import { DebtForm } from '../forms/DebtForm';
import { EducationFundingForm } from '../forms/EducationFundingForm';
import { AdministrationCostForm } from '../forms/AdministrationCostForm';
import { CharityForm } from '../forms/CharityForm';
import { LegacyForm } from '../forms/LegacyForm';

export class AddCustomerScreen extends Component {

  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);

    const { state } = this.props.navigation;
    let selected = state.params.selected;

    let profiles = [
      { name: 'Maintenance', value: 'maintenance' },
      { name: 'Debt', value: 'debt' },
      { name: 'Education Funding', value: 'education' },
      { name: 'Administration Cost', value: 'administration' },
      { name: 'Charity', value: 'charity' },
      { name: 'Legacy', value: 'legacy' },
    ];
    let index = _.findIndex(profiles, function(o) { return o.value == selected.value; });
    let selectedProfile = profiles[index];

    this.state = {
      customers: [],
      customer: {},
      profiles: profiles,
      profile: selectedProfile.value,
      selectedProfile: selectedProfile,
    };

    this.selectedProfile = this.selectedProfile.bind(this);
    this.goBack = this.goBack.bind(this);
    this.editCustomer = this.editCustomer.bind(this);
  }

  componentDidMount() {
    AsyncStorage.getItem(
      '@CustomerListStore:Customers', (err, data) => {
        if (err) {
          console.error('Error loading customers', err);
        } else {
          let customers = JSON.parse(data);
          if (Array.isArray(customers)) {
            this.setState({ customers });
          }
        }
    });
  }

  guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  saveCustomers(customers) {
    AsyncStorage.setItem(
      '@CustomerListStore:Customers',
      JSON.stringify(customers)
    ).then(res => {
      const { navigate } = this.props.navigation;
      navigate('Summary');
    });
  }

  editCustomer(data) {
    let type = this.state.profile;
    let name = type.substr(0, 1).toUpperCase() + type.substr(1);

    if(type == 'maintenance') {
      name = data.selected.title;
    }
    if(type == 'education') {
      name = 'Education Funding';
    }
    if(type == 'administration') {
      name = 'Administration Cost';
    }

    let customers = this.state.customers;
    let customer = {
      id: this.guid(),
      profile: this.state.profile,
      ... data,
      name: name,
      //home: customer.home
    };

    customers.push(customer);
    this.saveCustomers(customers);
  }

  selectedProfile(profile) {
    this.setState({ selectedProfile: profile, profile: profile.value });
  }

  goBack() {
    const { navigate } = this.props.navigation;
    navigate('Summary');
  }

  render() {
    return (
      <View style={{backgroundColor: 'white', flex: 1}}>
        <View style={{height: 70}}>
          <NavigationBar
            leftComponent={(
              <Button onPress={this.goBack}>
                <Icon name="back" />
              </Button>
            )}
            centerComponent={<Title>Add Data</Title>}
            rightComponent={
              <DropDownMenu
                ref="profiles"
                options={this.state.profiles}
                selectedOption={this.state.selectedProfile ? this.state.selectedProfile : this.state.profiles[0]}
                onOptionSelected={this.selectedProfile}
                titleProperty="name"
                valueProperty="value"
              />
            }
          />
        </View>

        <ScrollView>
          {this.state.profile == 'maintenance' &&
            <SpouseForm
              onPressEdit={(data) => this.editCustomer(data)}
              customer={{}} title='Maintenance' />
          }

          {this.state.profile == 'debt' &&
            <DebtForm
              onPressEdit={(data) => this.editCustomer(data)}
              customer={{}} title='Debt' />
          }

          {this.state.profile == 'education' &&
            <EducationFundingForm
              onPressEdit={(data) => this.editCustomer(data)}
              customer={{}} childrens='1' title='Education Funding' />
          }

          {this.state.profile == 'administration' &&
            <AdministrationCostForm
              onPressEdit={(data) => this.editCustomer(data)}
              customer={{}} title='Administration Cost' />
          }

          {this.state.profile == 'charity' &&
            <CharityForm
              onPressEdit={(data) => this.editCustomer(data)}
              customer={{}} title='Charity' />
          }

          {this.state.profile == 'legacy' &&
            <LegacyForm
              onPressEdit={(data) => this.editCustomer(data)}
              customer={{}} title='Legacy' />
          }
        </ScrollView>
      </View>
    )
  }
}
