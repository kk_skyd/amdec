import React, { Component } from 'react';
import { Alert, AsyncStorage, View, Text, StyleSheet, ScrollView } from 'react-native';
import { Button, DropDownModal, View as UiView, NavigationBar, Icon, Title } from '@shoutem/ui';

import { Customer } from '../components/Customer';
import { Customers } from '../components/Customers';
import { Email } from '../components/Email';
import { Information } from '../components/Information';
import { Summary } from '../components/Summary';

export class SummaryScreen extends Component {

  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);

    let forms = [
      { name: 'Maintenance', value: 'maintenance' },
      { name: 'Debt', value: 'debt' },
      { name: 'Education Funding', value: 'education' },
      { name: 'Administration Cost', value: 'administration' },
      { name: 'Charity', value: 'charity' },
      { name: 'Legacy', value: 'legacy' },
    ];

    this.state = {
      customers: [],
      home: {},

      forms: forms,
      selected: {},
      collapsed: false
    };

    this.goHome = this.goHome.bind(this);
    this.selectedAction = this.selectedAction.bind(this);

    this.onPressSelect = this.onPressSelect.bind(this);
    this.collapsed = this.collapsed.bind(this);
    this.close = this.close.bind(this);
  }

  componentDidMount() {
    AsyncStorage.getItem(
      '@CustomerListStore:Customers', (err, data) => {
        if (err) {
          console.error('Error loading customers', err);
        } else {
          let customers = JSON.parse(data);
          if (Array.isArray(customers)) {
            this.setState({ customers });
          }
        }
    });

    AsyncStorage.getItem(
      '@CustomerListStore:Home', (err, data) => {
        if (err) {
          console.error('Error loading home', err);
        } else {
          let home = JSON.parse(data);
          if (typeof home !== "undefined" && home != null  && typeof home.name !== "undefined") {
            this.setState({ home });
          }
        }
    });
  }

  saveCustomers(customers, route) {
    AsyncStorage.setItem(
      '@CustomerListStore:Customers',
      JSON.stringify(customers)
    ).then(res => {
      const { navigate } = this.props.navigation;
      navigate(route);
    });
  }

  selectedAction() {
    this.collapsed();
  }

  collapsed() {
    this.setState({ collapsed: true });
  }

  close() {
    this.setState({ collapsed: false });
  }

  onPressSelect(selected) {
    const { navigate } = this.props.navigation;
    navigate('AddCustomer', {selected});
  }

  goHome() {
    Alert.alert(
      'All data will be deleted!',
      'Are you sure?',
      [
        { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        { text: 'OK', onPress: () => this.saveCustomers([], 'Home') } ,
      ],
      { cancelable: false }
    );
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={{backgroundColor: 'white', flex: 1}}>
        <View style={{height: 70}}>
          <NavigationBar
            leftComponent={(
              <Button onPress={this.goHome}>
                <Icon name="home" />
              </Button>
            )}
            centerComponent={<Title>Summary</Title>}
            rightComponent={
              <Email ref='email' customers={this.state.customers}/>
            }
          />
        </View>

        <ScrollView>

          <Summary customers={this.state.customers} />

          <Button onPress={this.selectedAction} style={{backgroundColor: '#1980b9', padding: 10}}>
            <Text style={{color: '#fff', fontWeight:'700'}}>ADD PROFILE</Text>
          </Button>
          <DropDownModal
            options={this.state.forms}
            selectedOption={this.state.selected}
            onOptionSelected={this.onPressSelect}
            titleProperty="name"
            valueProperty="value"
            visible={this.state.collapsed}
            onClose={this.close}
          />

          <Information customers={this.state.customers} home={this.state.home} />

          <Customers customers={this.state.customers} navigate={navigate}/>
        </ScrollView>
      </View>
    )
  }
}
