import React, { Component } from 'react';
import {
  AsyncStorage, Alert,
  View, Text, StyleSheet, ScrollView
} from 'react-native';
import { Button, View as UiView, Screen, NavigationBar, Icon, Title } from '@shoutem/ui';

import _ from 'lodash';

import { HomeForm } from '../forms/HomeForm';
import { SpouseForm } from '../forms/SpouseForm';
import { DebtForm } from '../forms/DebtForm';
import { EducationFundingForm } from '../forms/EducationFundingForm';
import { AdministrationCostForm } from '../forms/AdministrationCostForm';
import { CharityForm } from '../forms/CharityForm';

import { DrawerNavigator, SafeAreaView } from 'react-navigation';

export class HomeScreen extends Component {

  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);

    this.state = {
      customers: [],
      isRefresh: true,
      isSelect: false,
      step: 0,
      steps: [
        {key: 0, type:'home', data: {}},
      ]
    };

    this.saveAllCustomers = this.saveAllCustomers.bind(this);
    this.saveHome = this.saveHome.bind(this);
    this.refresh = this.refresh.bind(this);
    this.onPressNavigationBarRight = this.onPressNavigationBarRight.bind(this);
    this.onPressDrawer = this.onPressDrawer.bind(this);

    this._handleAddStep = this._handleAddStep.bind(this);
    this._handleRemoveStep = this._handleRemoveStep.bind(this);
    this.onSumbitHome = this.onSumbitHome.bind(this);

    this.onPressDelete = this.onPressDelete.bind(this);
    this.onPressDeleteOk = this.onPressDeleteOk.bind(this);
    this.onPressBack = this.onPressBack.bind(this);
    this.onPressNext = this.onPressNext.bind(this);
    this.onPressSelect = this.onPressSelect.bind(this);
    this.nextLastStep = this.nextLastStep.bind(this);
  }

  componentDidMount() {
    AsyncStorage.getItem(
      '@CustomerListStore:Customers', (err, data) => {
        if (err) {
          console.error('Error loading customers', err);
        } else {
          let customers = JSON.parse(data);
          if (Array.isArray(customers)) {
            this.setState({ customers });
          }
        }
    });
  }

  guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  saveCustomers(customers, route) {
    AsyncStorage.setItem(
      '@CustomerListStore:Customers',
      JSON.stringify(customers)
    ).then(res => {
      const { navigate } = this.props.navigation;
      navigate(route);
    });
  }

  saveHome(home) {
    AsyncStorage.setItem(
      '@CustomerListStore:Home',
      JSON.stringify(home)
    );
  }

  saveAllCustomers(steps) {
    let customers = this.state.customers;
    let home = this.state.steps[0].data;

    steps.forEach(function(step) {
      let data = step.data;
      let type = step.type;
      let name = type.substr(0, 1).toUpperCase() + type.substr(1);

      if(type != 'home') {

        if(type == 'children') {
          name = 'Child';
        }
        if(type == 'education') {
          name = 'Education Funding';
        }
        if(type == 'administration') {
          name = 'Administration Cost';
        }

        if(type == 'dad' || type == 'mom' || type == 'children' || type == 'spouse') {
          type = 'maintenance';
          data.yearly = data.monthly * 12;
        }

        let customer = {
          id: step.id,
          profile: type,
          ... data,
          name: name,
        };
        customers.push(customer);
      }
    });

    let homeStore = {
      name: home.name,
      male: home.male,
      female: home.female,
      dob: home.dob,
      age: home.age,
      hp: home.hp,
      contact: home.contact,
    };
    this.saveHome(homeStore);
    
    this.saveCustomers(customers, 'Summary');
  }

  refresh() {
    Alert.alert(
      'Delete all data!',
      'Are you sure?',
      [
        { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        { text: 'OK', onPress: () => this.saveCustomers([], 'Home') } ,
      ],
      { cancelable: false }
    );
  }

  onPressNavigationBarRight() {
    if(this.state.isRefresh) {
      this.refresh();
    } else {
      const { navigate } = this.props.navigation;
      navigate('Summary');
    }
  }

  onPressDrawer() {
    const { navigate } = this.props.navigation;
    navigate('DrawerOpen');
  }

  _handleRefresh(step) {
    let isRefresh = this.state.isRefresh;

    if(step == 0) {
      isRefresh = true;
    } else {
      isRefresh = false;
    }

    return isRefresh;
  }

  _generatorForm(steps, type, data) {
    let key = this.guid();
    let newly_added_data = {
      /* psedo code to simulate key auto increment */
      // key: steps[steps.length-1].key+1,
      key: key,
      id: key,
      type: type,
      data: data
    };
    return newly_added_data;
  }

  _handleAddStep(type, data) {
    let steps = this.state.steps;
    let newly_added_data = this._generatorForm(steps, type, data);
    steps = [...steps, newly_added_data];

    let step = this.state.step;
    let isSelect = true;
    this.setState({ steps, isSelect }, function() {
      let index = this._findIndexStep(steps, step);
      this.refs[steps[index].key].onPressNext();
    });
  }

  _handleRemoveStep(key) {
    let steps = this.state.steps;
    let step = this._prevStep(steps);
    steps = steps.filter( (data) => data.key !== key );
    this.setState({ steps, step });
  }

  _findIndexStep(steps, key) {
    return _.findIndex(steps, { 'key': key });
  }

  _findStep(steps, type) {
    return _.find(steps, { 'type': type });
  }

  _findSteps(steps, type) {
    let finds = [];
    steps.forEach(function(step) {
      if(step.type == type) {
        finds.push(step);
      }
    });
    return finds;
  }

  _nextStep(steps) {
    let currentStep = this.state.step;
    let nextStep = null;
    let founded = false;

    _.forEach(steps, function(step) {
      if(founded) {
        nextStep = step.key;
        founded = false;
      }

      if(currentStep == step.key) {
        founded = true;
      }
    });

    return nextStep;
  }

  _prevStep(steps) {
    let currentStep = this.state.step;
    let preStep = null;
    let stoped = false;

    _.forEach(steps, function(step) {
      if(currentStep == step.key) {
        stoped = true;
      }

      if(!stoped) {
        preStep = step.key;
      }
    });

    return preStep;
  }

  _lastStep(steps) {
    let lastStep = _.last(steps);
    return lastStep.key;
  }

  onSumbitHome(key, homeForm) {
    let steps = this.state.steps;
    let index = this._findIndexStep(steps, key);
    steps[index].data = homeForm;
    let newlyData = '';

    // Generator form
    let dad = this._findStep(steps, 'dad');
    if(homeForm.dad) {
      if(!dad) {
        newlyData = this._generatorForm(steps, 'dad', {});
        steps = [...steps, newlyData];
      }
    } else {
      if(dad) {
        steps = steps.filter( (data) => data.key !== dad.key );
      }
    }

    let mom = this._findStep(steps, 'mom');
    if(homeForm.mom) {
      if(!mom) {
        newlyData = this._generatorForm(steps, 'mom', {});
        steps = [...steps, newlyData];
      }
    } else {
      if(mom) {
        steps = steps.filter( (data) => data.key !== mom.key );
      }
    }

    let spouse = this._findStep(steps, 'spouse');
    if(homeForm.spouse) {
      if(!spouse) {
        newlyData = this._generatorForm(steps, 'spouse', {});
        steps = [...steps, newlyData];
      }
    } else {
      if(spouse) {
        steps = steps.filter( (data) => data.key !== spouse.key );
      }
    }

    let selectChilds = parseInt(homeForm.selectedChild.value);
    let childrens = this._findSteps(steps, 'children');
    if(selectChilds > childrens.length) {
      let childs = selectChilds - childrens.length;
      for (let i = 1; i <= childs; i++) {
        newlyData = this._generatorForm(steps, 'children', {});
        steps = [...steps, newlyData];
      }
    } else {
      let childs = childrens.length - selectChilds;
      for (let i = 1; i <= childs; i++) {
        newlyData = childrens[childrens.length-1];
        childrens.pop();
        steps = steps.filter( (data) => data.key !== newlyData.key );
      }
    }

    // Set state
    let step = this._nextStep(steps);
    let isRefresh = this._handleRefresh(step);
    this.setState({ steps, step, isRefresh });
  }

  onPressDelete() {
    Alert.alert(
      'Delete form!',
      'Are you sure?',
      [
        { text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        { text: 'OK', onPress: () => this.onPressDeleteOk() } ,
      ],
      { cancelable: false }
    );
  }

  onPressDeleteOk() {
    let key = this.state.step;
    this._handleRemoveStep(key);
  }

  onPressBack(key, form) {
    let steps = this.state.steps;
    let index = this._findIndexStep(steps, key);
    steps[index].data = form;
    let step = this._prevStep(steps);
    let isRefresh = this._handleRefresh(step);
    this.setState({ steps, step, isRefresh });
  }

  nextLastStep() {
    let steps = this.state.steps;
    let step = this.state.step;
    let isSelect = this.state.isSelect;
    let nextStep = this._nextStep(steps);

    if(!nextStep) {
      isSelect = false;
      this.setState({ isSelect });
    }

    if(isSelect) {
      let index = this._findIndexStep(steps, step);
      this.refs[steps[index].key].onPressNext();
    }
  }

  onPressNext(key, form) {
    let steps = this.state.steps;
    let index = this._findIndexStep(steps, key);
    steps[index].data = form;
    let step = this._nextStep(steps);
    let isRefresh = this._handleRefresh(step);

    // Finish step
    if(!step) {
      this.saveAllCustomers(steps);
    }

    // Save state
    this.setState({ steps, step, isRefresh }, () => {
      this.nextLastStep();
    });
    // this.setState({ steps, step, isRefresh });
  }

  onPressSelect(value) {
    if(value == 'Debt') {
      this._handleAddStep('debt', {});
    }

    if(value == 'EducationFunding') {
      this._handleAddStep('education', {});
    }

    if(value == 'AdministrationCost') {
      this._handleAddStep('administration', {});
    }

    if(value == 'Charity') {
      this._handleAddStep('charity', {});
    }
  }

  render() {
    let steps = this.state.steps.map((data, index) => {
      if(data.type == 'home') {
        return (
          <View key={index} style={[styles.screen, data.key != this.state.step && styles.hide]}>
            <HomeForm data={data}
              onSubmit={(state) => this.onSumbitHome(data.key, state)}
              />
          </View>
        );
      }

      if(data.type == 'spouse') {
        return (
          <View key={index} style={[styles.screen, data.key != this.state.step && styles.hide]}>
            <SpouseForm ref={data.key} title='Spouse'
              onPressDelete={this.onPressDelete}
              onPressBack={(state) => this.onPressBack(data.key, state)}
              onPressNext={(state) => this.onPressNext(data.key, state)}
              onPressSelect={this.onPressSelect}
              />
          </View>
        );
      }

      if(data.type == 'dad') {
        return (
          <View key={index} style={[styles.screen, data.key != this.state.step && styles.hide]}>
            <SpouseForm ref={data.key} title='Dad'
              onPressDelete={this.onPressDelete}
              onPressBack={(state) => this.onPressBack(data.key, state)}
              onPressNext={(state) => this.onPressNext(data.key, state)}
              onPressSelect={this.onPressSelect}
              />
          </View>
        )
      }

      if(data.type == 'mom') {
        return (
          <View key={index} style={[styles.screen, data.key != this.state.step && styles.hide]}>
            <SpouseForm ref={data.key} title='Mom'
              onPressDelete={this.onPressDelete}
              onPressBack={(state) => this.onPressBack(data.key, state)}
              onPressNext={(state) => this.onPressNext(data.key, state)}
              onPressSelect={this.onPressSelect}
              />
          </View>
        )
      }

      if(data.type == 'children') {
        return (
          <View key={index} style={[styles.screen, data.key != this.state.step && styles.hide]}>
            <SpouseForm ref={data.key} title='Children'
              onPressDelete={this.onPressDelete}
              onPressBack={(state) => this.onPressBack(data.key, state)}
              onPressNext={(state) => this.onPressNext(data.key, state)}
              onPressSelect={this.onPressSelect}
              />
          </View>
        )
      }

      if(data.type == 'debt') {
        return (
          <View key={index} style={[styles.screen, data.key != this.state.step && styles.hide]}>
            <DebtForm ref={data.key} title='Debt'
              onPressDelete={this.onPressDelete}
              onPressBack={(state) => this.onPressBack(data.key, state)}
              onPressNext={(state) => this.onPressNext(data.key, state)}
              onPressSelect={this.onPressSelect}
              />
          </View>
        );
      }

      if(data.type == 'education') {
        return (
          <View key={index} style={[styles.screen, data.key != this.state.step && styles.hide]}>
            <EducationFundingForm ref={data.key} title='Education Funding'
              onPressDelete={this.onPressDelete}
              childrens={this.state.steps[0].data.selectedChild.value}
              onPressBack={(state) => this.onPressBack(data.key, state)}
              onPressNext={(state) => this.onPressNext(data.key, state)}
              onPressSelect={this.onPressSelect}
              />
          </View>
        );
      }

      if(data.type == 'administration') {
        return (
          <View key={index} style={[styles.screen, data.key != this.state.step && styles.hide]}>
            <AdministrationCostForm ref={data.key} title='Administration Cost'
              onPressDelete={this.onPressDelete}
              onPressBack={(state) => this.onPressBack(data.key, state)}
              onPressNext={(state) => this.onPressNext(data.key, state)}
              onPressSelect={this.onPressSelect}
              />
          </View>
        );
      }

      if(data.type == 'charity') {
        return (
          <View key={index} style={[styles.screen, data.key != this.state.step && styles.hide]}>
            <CharityForm ref={data.key} title='Charity Allocation'
              onPressDelete={this.onPressDelete}
              onPressBack={(state) => this.onPressBack(data.key, state)}
              onPressNext={(state) => this.onPressNext(data.key, state)}
              onPressSelect={this.onPressSelect}
              />
          </View>
        );
      }
    });

    return (
      <View style={{backgroundColor: 'white', flex: 1}}>
        <View style={{height: 70}}>
          <NavigationBar
            leftComponent={(<Button onPress={this.onPressDrawer}><Icon name="sidebar" /></Button>)}
            centerComponent={<Title>AMDEC</Title>}
            rightComponent={(
              <Button onPress={this.onPressNavigationBarRight}>
                {this.state.isRefresh &&
                  <Icon name="refresh" />
                }

                {!this.state.isRefresh &&
                  <Text>Summary</Text>
                }
              </Button>
            )}
          />
        </View>
        {steps}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  screen: {
    flex: 1
  },
  hide: {
    display: 'none'
  }
});
