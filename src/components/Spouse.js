import React, { Component } from 'react';
import { StyleSheet, Text } from 'react-native';

import { Button, View, Divider } from '@shoutem/ui';

export class Spouse extends Component {

  constructor(props) {
    super(props);

    this.state = {
      customer: this.props.customer
    };
  }

  render() {
    let customer = this.state.customer;
    let amount = (customer.amount * 1).toLocaleString('en');
    let name = `${customer.name} (Age ${customer.age} - support until age ${customer.ageUtil})`;

    return (
      <View style={{alignItems: 'center'}}>
        <Text style={styles.title}>{name}</Text>
        <Text style={styles.text}>Monthly {(customer.monthly * 1).toLocaleString('en')}</Text>
        <Text style={styles.text}>Yearly {(customer.yearly * 1).toLocaleString('en')}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    marginBottom: 10,
    fontWeight: 'bold',
    fontSize: 18,
  },
  text: {
    marginBottom: 5,
    fontSize: 16,
  },
});
