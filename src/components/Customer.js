import React, { Component } from 'react';
import { StyleSheet, ScrollView } from 'react-native';

import { TouchableOpacity, Button, View, Title, Text, Divider } from '@shoutem/ui';

import { Debt } from '../components/Debt';
import { Education } from '../components/Education';
import { Administration } from '../components/Administration';
import { Charity } from '../components/Charity';
import { Spouse } from '../components/Spouse';
import { Legacy } from '../components/Legacy';

export class Customer extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    let navigate = this.props.navigate;
    let customer = this.props.customer;
    let amount = (customer.amount * 1).toLocaleString('en');

    return (
      <View style={{backgroundColor: 'white'}}>
        <View style={{padding: 10}}>
          <TouchableOpacity onPress={() => navigate('Customer', {customer})}>
            {customer.profile == 'maintenance' &&
              <Spouse customer={customer} />
            }

            {customer.profile == 'debt' &&
              <Debt customer={customer} />
            }

            {customer.profile == 'education' &&
              <Education customer={customer} />
            }

            {customer.profile == 'administration' &&
              <Administration customer={customer} />
            }

            {customer.profile == 'charity' &&
              <Charity customer={customer} />
            }

            {customer.profile == 'legacy' &&
              <Legacy customer={customer} />
            }
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
