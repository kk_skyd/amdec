import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';

import { TouchableOpacity, Button, Title, Divider } from '@shoutem/ui';
import { Customer } from '../components/Customer';

export class Customers extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loaded: false,
      navigate: {},
      customers: [],
    };
  }

  componentWillReceiveProps(nextProps) {
    let customers = nextProps.customers;
    let navigate = nextProps.navigate;
    let loaded = true;
    this.setState({ customers, navigate, loaded });
  }

  renderList(customers) {
    for (let customer of customers) {
      return (
        <Customer customer={customer} />
      );
    }
  }

  render() {
    if(this.state.loaded) {
      let total = 0;
      let sumary = {maintenance: 0, debt: 0, education: 0, charity: 0, administration: 0, legacy: 0};

      let maintenance = [];
      let debt = [];
      let education = [];
      let charity = [];
      let administration = [];
      let legacy = [];

      let customers = this.state.customers;
      for (let customer of customers) {
        total = customer.amount * 1;
        switch (customer.profile) {
          case 'maintenance':
            sumary.maintenance += total;
            maintenance.push(customer);
            break;

          case 'debt':
            sumary.debt += total;
            debt.push(customer);
            break;

          case 'education':
            sumary.education += total;
            education.push(customer);
            break;

          case 'charity':
            sumary.charity += total;
            charity.push(customer);
            break;

          case 'administration':
            sumary.administration += total;
            administration.push(customer);
            break;

          case 'legacy':
            sumary.legacy += total;
            legacy.push(customer);
            break;
        }
      }

      let navigate = this.state.navigate;

      return (
        <View>

          {sumary.maintenance > 0 &&
            <View>
              <View style={styles.container}>
                <View style={styles.title}>
                  <Text style={styles.top1}>MAINTENANCE</Text>
                </View>
                <View style={styles.total}>
                  <Text style={styles.top2}>TOTAL: {(sumary.maintenance * 1).toLocaleString('en')}</Text>
                </View>
              </View>
              <View style={styles.list}>
                {
                  maintenance.map(( customer, key ) =>
                  (
                    <Customer customer={customer} navigate={navigate} key={key} />
                  ))
                }
              </View>
            </View>
          }

          {sumary.debt > 0 &&
            <View>
              <View style={styles.container}>
                <View style={styles.title}>
                  <Text style={styles.top1}>DEBT</Text>
                </View>
                <View style={styles.total}>
                  <Text style={styles.top2}>TOTAL: {(sumary.debt * 1).toLocaleString('en')}</Text>
                </View>
              </View>
              <View style={styles.list}>
                {
                  debt.map(( customer, key ) =>
                  (
                    <Customer customer={customer} navigate={navigate} key={key} />
                  ))
                }
              </View>
            </View>
          }

          {sumary.education > 0 &&
            <View>
              <View style={styles.container}>
                <View style={styles.title}>
                  <Text style={styles.top1}>EDUCATION</Text>
                </View>
                <View style={styles.total}>
                  <Text style={styles.top2}>TOTAL: {(sumary.education * 1).toLocaleString('en')}</Text>
                </View>
              </View>
              <View style={styles.list}>
                {
                  education.map(( customer, key ) =>
                  (
                    <Customer customer={customer} navigate={navigate} key={key} />
                  ))
                }
              </View>
            </View>
          }

          {sumary.charity > 0 &&
            <View>
              <View style={styles.container}>
                <View style={styles.title}>
                  <Text style={styles.top1}>CHARITY</Text>
                </View>
                <View style={styles.total}>
                  <Text style={styles.top2}>TOTAL: {(sumary.charity * 1).toLocaleString('en')}</Text>
                </View>
              </View>
              <View style={styles.list}>
                {
                  charity.map(( customer, key ) =>
                  (
                    <Customer customer={customer} navigate={navigate} key={key} />
                  ))
                }
              </View>
            </View>
          }

          {sumary.administration > 0 &&
            <View>
              <View style={styles.container}>
                <View style={styles.title}>
                  <Text style={styles.top1}>ADMINISTRATION</Text>
                </View>
                <View style={styles.total}>
                  <Text style={styles.top2}>TOTAL: {(sumary.administration * 1).toLocaleString('en')}</Text>
                </View>
              </View>
              <View style={styles.list}>
                {
                  administration.map(( customer, key ) =>
                  (
                    <Customer customer={customer} navigate={navigate} key={key} />
                  ))
                }
              </View>
            </View>
          }

          {sumary.legacy > 0 &&
            <View>
              <View style={styles.container}>
                <View style={styles.title}>
                  <Text style={styles.top1}>LEGACY</Text>
                </View>
                <View style={styles.total}>
                  <Text style={styles.top2}>TOTAL: {(sumary.legacy * 1).toLocaleString('en')}</Text>
                </View>
              </View>
              <View style={styles.list}>
                {
                  legacy.map(( customer, key ) =>
                  (
                    <Customer customer={customer} navigate={navigate} key={key} />
                  ))
                }
              </View>
            </View>
          }

        </View>
      );
    } else {
      return (
        <View style={{margin: 15}}></View>
      );
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingVertical: 9,
    paddingHorizontal: 9,
    backgroundColor: '#6699FF',
  },
  title: {
  },
  total: {
    flex: 1,
  },
  top1: {
    color: '#fff',
    fontSize: 16,
  },
  top2: {
    color: '#fff',
    fontSize: 16,
    textAlign: 'right',
  },
  list: {
    flexDirection: 'column',
  },
});
