import React, { Component } from 'react';
import { StyleSheet, View, Text } from 'react-native';

import { Button, Title, Divider } from '@shoutem/ui';

export class Information extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loaded: false,
      customers: [],
      home: {},
      sumary: {}
    };
  }

  componentWillReceiveProps(nextProps) {
    let customers = nextProps.customers;
    let home = nextProps.home;

    let sumary = this.sumary(customers);
    let loaded = true;

    this.setState({ customers, home, sumary, loaded });
  }

  sumary(customers) {
    let total = 0;
    let sumary = {
      total: 0,
      maintenance: 0,
      debt: 0,
      education: 0,
      charity: 0,
      administration: 0,
      legacy: 0,
    };

    for (let customer of customers) {
      total = customer.amount * 1;
      switch (customer.profile) {
        case 'maintenance':
          sumary.maintenance += total;
          break;

        case 'debt':
          sumary.debt += total;
          break;

        case 'education':
          sumary.education += total;
          break;

        case 'charity':
          sumary.charity += total;
          break;

        case 'administration':
          sumary.administration += total;
          break;

        case 'legacy':
          sumary.legacy += total;
          break;
      }
      sumary.total += total;
    }

    return sumary;
  }

  render() {
    if(this.state.loaded) {
      let home = this.state.home;
      let sumary = this.state.sumary;
      let maintenance = (sumary.maintenance * 100 / sumary.total).toFixed(2);
      let debt = (sumary.debt * 100 / sumary.total).toFixed(2);
      let education = (sumary.education * 100 / sumary.total).toFixed(2);
      let charity = (sumary.charity * 100 / sumary.total).toFixed(2);
      let administration = (sumary.administration * 100 / sumary.total).toFixed(2);
      let legacy = (sumary.legacy * 100 / sumary.total).toFixed(2);
      let total = (sumary.total * 1).toLocaleString('en');

      let gender = 'MALE';
      if (home.female) {
        gender = 'FEMALE';
      }

      let from = home.dob + "";
      from = from.split("-");
      let age = (new Date()).getFullYear() - from[0];

      return (
        <View style={styles.all}>

          <View style={styles.container}>
            <View style={styles.left}>
              <Text style={styles.text}>NAME</Text>
              <Text style={styles.text}>H/P</Text>
              <Text style={styles.text}>DOB</Text>
              <Text style={styles.text}>AGE</Text>
              <Text style={styles.text}>GENDER</Text>
            </View>
            <View style={styles.right}>
              <Text style={styles.text2}>{home.name}</Text>
              <Text style={styles.text2}>{home.hp}</Text>
              <Text style={styles.text2}>{home.dob}</Text>
              <Text style={styles.text2}>{age}</Text>
              <Text style={styles.text2}>{gender}</Text>
            </View>
          </View>

          <View style={{ width: 10 }}></View>

          <View style={styles.container}>
            <View style={styles.left}>
              <Text style={styles.text}>Maintenance</Text>
              <Text style={styles.text}>Debt</Text>
              <Text style={styles.text}>Education</Text>
              <Text style={styles.text}>Charity</Text>
              <Text style={styles.text}>Administration</Text>
              <Text style={styles.text}>Legacy</Text>
              <Title>TOTAL</Title>
            </View>
            <View style={styles.right}>
              <Text style={styles.text2}>{maintenance}%</Text>
              <Text style={styles.text2}>{debt}%</Text>
              <Text style={styles.text2}>{education}%</Text>
              <Text style={styles.text2}>{charity}%</Text>
              <Text style={styles.text2}>{administration}%</Text>
              <Text style={styles.text2}>{legacy}%</Text>
              <Title>RM {total}</Title>
            </View>
          </View>

        </View>
      );
    } else {
      return (
        <View style={{margin: 15}}></View>
      );
    }
  }
}

const styles = StyleSheet.create({
  all: {
    flexDirection: 'row',
    paddingVertical: 9,
    paddingHorizontal: 9,
  },
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  left: {
    flex: 1,
    backgroundColor: '#BDD8EE',
    padding: 10,
  },
  right: {
    flex: 1,
    backgroundColor: '#E9EBF5',
    padding: 10,
  },
  text: {
    marginBottom: 10,
    fontWeight: 'bold',
    fontSize: 14,
  },
  text2: {
    marginBottom: 10,
    fontSize: 14,
  },
});
