import React, { Component } from 'react';
import { View, Text, Alert, StyleSheet, TouchableOpacity } from 'react-native';

import { Button, Icon } from '@shoutem/ui';

import RNHTMLtoPDF from 'react-native-html-to-pdf';
import Share from 'react-native-share';

export class Email extends Component {

  constructor(props) {
    super(props);

    this.state = {
      customers: [],
      sumary: {}
    };

    this.createPdf = this.createPdf.bind(this);
    this.sendMail = this.sendMail.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    let customers = nextProps.customers;
    let sumary = this.sumary(customers);
    this.setState({ customers, sumary });
  }

  sumary(customers) {
    let total = 0;
    let sumary = {
      total: 0,
      maintenance: 0,
      debt: 0,
      education: 0,
      charity: 0,
      administration: 0,
      legacy: 0,
    };

    for (let customer of customers) {
      total = customer.amount * 1
      switch (customer.profile) {
        case 'debt':
          sumary.debt += total
          break;

        case 'maintenance':
          sumary.maintenance += total
          break;

        case 'education':
          sumary.education += total
          break;

        case 'charity':
          sumary.charity += total
          break;

        case 'administration':
          sumary.administration += total
          break;

        case 'legacy':
          sumary.legacy += total
          break;
      }
      sumary.total += total
    }
    return sumary;
  }

  async createPdf() {
    const { customers, sumary } = this.state;
    let html = '';

    customers.forEach(function(customer) {
      let amount = (customer.amount * 1).toLocaleString('en');
      switch (customer.profile) {
        case "debt":
          html += `
            <div>
              <p>${customer.name}</p>
              <p>${customer.selected.title} ${amount}</p>
            </div>
            <hr/>
          `;
          break;

        case "education":
          html += `
            <div>
              <p>${customer.name}</p>
              <p>
                Number of children ${(customer.educationChildrens * 1).toLocaleString('en')} <br/>
                stimated Education Fees /pax ${(customer.educationFees * 1).toLocaleString('en')}
              </p>
              <p>Total Education Funding ${amount}</p>
            </div>
            <hr/>
          `;
          break;

        case "administration":
          html += `
            <div>
              <p>${customer.name}</p>
              <p>
                Estate Amount ${(customer.administrationEstateAmount * 1).toLocaleString('en')} <br/>
                (Normarly 10% of Estate Amount)
              </p>
              <p>Total Administration Cost ${amount}</p>
            </div>
            <hr/>
          `;
          break;

        case "charity":
          html += `
            <div>
              <p>${customer.name}</p>
              <p>
                Contribution ${(customer.charityYearlyContribution * 1).toLocaleString('en')} <br/>
                Number of Years ${(customer.charityYears * 1).toLocaleString('en')}
              </p>
              <p>Total Contribution ${amount}</p>
            </div>
            <hr/>
          `;
          break;

        default:
          html += `
            <div>
              <p>${customer.name} (Age ${customer.age} - support until age ${customer.ageUtil})</p>
              <p>
                Monthly ${(customer.monthly * 1).toLocaleString('en')} <br/>
                Yearly ${(customer.yearly * 1).toLocaleString('en')}
              </p>
              <p>Total Allocation ${amount}</p>
            </div>
            <hr/>
          `;
          break;
      }
    });

    let maintenance = (sumary.maintenance * 100 / sumary.total).toFixed(2);
    let debt = (sumary.debt * 100 / sumary.total).toFixed(2);
    let education = (sumary.education * 100 / sumary.total).toFixed(2);
    let charity = (sumary.charity * 100 / sumary.total).toFixed(2);
    let administration = (sumary.administration * 100 / sumary.total).toFixed(2);
    let total = (sumary.total * 1).toLocaleString('en');

    let grath = `
      <h2>Summary</h2>
      <h3>Total ${total}</h3>
      <p>
        Maintenance ${maintenance}% <br/>
        Debt ${debt}% <br/>
        Education ${education}% <br/>
        Charity ${charity}% <br/>
        Administration ${administration}%
      </p>
    `;

    let options = {
      html: '<h1>Customers</h1>' + html + grath,
      //fileName: 'customers',
      //directory: 'docs',
      // directory: RNFS.DocumentDirectoryPath,
      //base64: true
    };

    let file = await RNHTMLtoPDF.convert(options);
    return file.filePath;
  }

  sendMail() {
    this.createPdf().then(filePath => {
      Share.open({
          title: 'Share the pdf file?',
          message: 'Email or Download',
          url: 'file://' + filePath
      }).catch(err => console.log(err));
    }).catch(err => alert(err));
  }

  render() {
    return (
      <View>
        <Button onPress={this.sendMail}>
          <Icon name="email" />
        </Button>
      </View>
    );
  }
}
