import React, { Component } from 'react';
import { StyleSheet, Text } from 'react-native';

import { Button, View, Divider } from '@shoutem/ui';

export class Debt extends Component {

  constructor(props) {
    super(props);

    this.state = {
      customer: this.props.customer
    };
  }

  render() {
    let customer = this.state.customer;
    let amount = (customer.amount * 1).toLocaleString('en');

    return (
      <View style={{alignItems: 'center'}}>
        <Text style={styles.text}>{customer.selected.title} {amount.toLocaleString('en')}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    marginBottom: 10,
    fontWeight: 'bold',
    fontSize: 18,
  },
  text: {
    fontSize: 16,
  },
});
