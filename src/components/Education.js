import React, { Component } from 'react';
import { StyleSheet, Text } from 'react-native';

import { Button, View } from '@shoutem/ui';

export class Education extends Component {

  constructor(props) {
    super(props);

    this.state = {
      customer: this.props.customer
    };
  }

  render() {
    let customer = this.state.customer;
    let amount = (customer.amount * 1).toLocaleString('en');

    return (
      <View style={{alignItems: 'center'}}>
        <Text style={styles.text}>Number of children {(customer.educationChildrens * 1).toLocaleString('en')}</Text>
        <Text style={styles.text}>Estimated Education Fees/pax {(customer.educationFees * 1).toLocaleString('en')}</Text>
        <Text style={styles.text}>Total {amount}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    marginBottom: 10,
    fontWeight: 'bold',
    fontSize: 18,
  },
  text: {
    marginBottom: 5,
    fontSize: 16,
  },
});
