import React, { Component } from 'react';
import { StyleSheet } from 'react-native';

import { Button, View, Title, Text, Divider } from '@shoutem/ui';
import { VictoryPie, VictoryTheme } from "victory-native";

export class Summary extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loaded: false,
      customers: [],
      sumary: {}
    };
  }

  componentWillReceiveProps(nextProps) {
    let customers = nextProps.customers;
    let sumary = this.sumary(customers);
    let loaded = true;
    this.setState({ customers, sumary, loaded });
  }

  sumary(customers) {
    let total = 0;
    let sumary = {
      total: 0,
      maintenance: 0,
      debt: 0,
      education: 0,
      charity: 0,
      administration: 0,
      legacy: 0,
    };

    for (let customer of customers) {
      total = customer.amount * 1;
      switch (customer.profile) {
        case 'maintenance':
          sumary.maintenance += total;
          break;

        case 'debt':
          sumary.debt += total;
          break;

        case 'education':
          sumary.education += total;
          break;

        case 'charity':
          sumary.charity += total;
          break;

        case 'administration':
          sumary.administration += total;
          break;

        case 'legacy':
          sumary.legacy += total;
          break;
      }
      sumary.total += total;
    }

    return sumary;
  }

  render() {
    if(this.state.loaded) {
      let sumary = this.state.sumary;
      let maintenance = (sumary.maintenance * 100 / sumary.total).toFixed(2);
      let debt = (sumary.debt * 100 / sumary.total).toFixed(2);
      let education = (sumary.education * 100 / sumary.total).toFixed(2);
      let charity = (sumary.charity * 100 / sumary.total).toFixed(2);
      let administration = (sumary.administration * 100 / sumary.total).toFixed(2);
      let legacy = (sumary.legacy * 100 / sumary.total).toFixed(2);
      let total = (sumary.total * 1).toLocaleString('en');

      let data = [];
      if(sumary.maintenance) {
        data.push({
          "x": "Maintenance",
          "y": sumary.maintenance
        });
      }

      if(sumary.debt) {
        data.push({
          "x": "Debt",
          "y": sumary.debt
        });
      }

      if(sumary.education) {
        data.push({
          "x": "Education",
          "y": sumary.education
        });
      }

      if(sumary.charity) {
        data.push({
          "x": "Charity",
          "y": sumary.charity
        });
      }

      if(sumary.administration) {
        data.push({
          "x": "Admin",
          "y": sumary.administration
        });
      }

      if(sumary.legacy) {
        data.push({
          "x": "Legacy",
          "y": sumary.legacy
        });
      }

      let options = {
        margin: {
          top: 0,
          left: 0,
          right: 0,
          bottom: 0
        },
        width: 350,
        height: 350,
        color: '#2980B9',
        r: 50,
        R: 150,
        legendPosition: 'topLeft',
        animate: {
          type: 'oneByOne',
          duration: 200,
          fillTransition: 3
        },
        label: {
          fontFamily: 'Arial',
          fontSize: 8,
          fontWeight: true,
          color: '#ECF0F1'
        }
      }

      return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#fff'}}>
            <VictoryPie
              theme={VictoryTheme.material}
              animate={{duration: 1500}}
              labelRadius={90}
              style={{ labels: { fill: "#000", fontSize: 12 } }}
              data={data}
              />
        </View>
      );
    } else {
      return (
        <View style={{margin: 15}}></View>
      );
    }
  }
}
